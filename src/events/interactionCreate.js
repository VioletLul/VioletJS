const { Interaction } = require("discord.js");

module.exports = {
    name: 'interactionCreate',
    async execute(interaction, client) {
        if (!interaction.isCommand()) return;

        const command = client.commands.get(interaction.commandName);

        if (!command) return
        
        try{


            await command.execute(interaction, client);
        } catch (error) {
            console.log(error);
            await interaction.reply({
                content: 'Hm, anscheinend hat hier etwas nicht funktioniert. Bitte informiere ^^Violet#0461 darüber! \nFehlercode: 00x22x2', 
                ephemeral: true
            });
        } 

    },
    


};