const { SlashCommandBuilder } = require("@discordjs/builders");
const { PermissionsBitField, ButtonStyle, ActionRowBuilder, EmbedBuilder, ButtonBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
    .setName('reactionrole5')
    .setDescription('Reaktionsrollen Befehl')
    .addRoleOption(option => option.setName(`role1`).setDescription("Diese Rolle wählst du als erstes aus.").setRequired(true))
    .addRoleOption(option => option.setName(`role2`).setDescription("Diese Rolle wählst du als zweites aus.").setRequired(true))
    .addRoleOption(option => option.setName(`role3`).setDescription("Diese Rolle wählst du als drittes aus.").setRequired(true))
    .addRoleOption(option => option.setName(`role4`).setDescription("Diese Rolle wählst du als viertes aus.").setRequired(true))
    .addRoleOption(option => option.setName(`role5`).setDescription("Diese Rolle wählst du als fünftes aus.").setRequired(true)),
    async execute (interaction, client) {

        const role1 = interaction.options.getRole('role1')
        const role2 = interaction.options.getRole('role2')
        const role3 = interaction.options.getRole('role3')
        const role4 = interaction.options.getRole('role4')
        const role5 = interaction.options.getRole('role5');
 if (!interaction.member.permissions.has(PermissionsBitField.Flags.Administrator)) return await interaction.reply({ content: "Hm, anscheinend bist du nicht mächtig genug um diese Nachricht zu beschwören!", ephemeral: true});
       
        
        const button = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId("button1")
            .setLabel(`${role1.name}`)
            .setStyle(ButtonStyle.Secondary),

            new ButtonBuilder()
            .setCustomId("button2")
            .setLabel(`${role2.name}`)
            .setStyle(ButtonStyle.Secondary),

            new ButtonBuilder()
            .setCustomId("button3")
            .setLabel(`${role3.name}`)
            .setStyle(ButtonStyle.Secondary),

            new ButtonBuilder()
            .setCustomId("button4")
            .setLabel(`${role4.name}`)
            .setStyle(ButtonStyle.Secondary),

            new ButtonBuilder()
            .setCustomId("button5")
            .setLabel(`${role5.name}`)
            .setStyle(ButtonStyle.Secondary)
        )
         
        const embed = new EmbedBuilder()
            .setColor("Blurple")
            .setTitle("Rollen? ROLLEN!")
            .setDescription(`Da, schau nach unten`)
            
            await interaction.reply({ embeds: [embed], components: [button]});

            const collector = await interaction.channel.createMessageComponentCollector();

            collector.on('collect', async (i) =>{

                const member = i.member;

                if (i.guild.members.me.roles.highest.position < role1.position) {
                    i.update({ content: "Die Rollen die ich vergeben soll sind zu mächtig. Ich habe diese Reaktion bis zum beheben des Fehlers ausgeschaltet.", ephemeral: true});
                    return;
                }else if (i.guild.members.me.roles.highest.position < role2.position) {
                    i.update({ content: "Die Rollen die ich vergeben soll sind zu mächtig. Ich habe diese Reaktion bis zum beheben des Fehlers ausgeschaltet.", ephemeral: true});
                    return;
                }else if (i.guild.members.me.roles.highest.position < role3.position) {
                    i.update({ content: "Die Rollen die ich vergeben soll sind zu mächtig. Ich habe diese Reaktion bis zum beheben des Fehlers ausgeschaltet.", ephemeral: true});
                    return;
                }else if (i.guild.members.me.roles.highest.position < role4.position) {
                    i.update({ content: "Die Rollen die ich vergeben soll sind zu mächtig. Ich habe diese Reaktion bis zum beheben des Fehlers ausgeschaltet.", ephemeral: true});
                    return;
                }else if (i.guild.members.me.roles.highest.position < role5.position) {
                    i.update({ content: "Die Rollen die ich vergeben soll sind zu mächtig. Ich habe diese Reaktion bis zum beheben des Fehlers ausgeschaltet.", ephemeral: true});
                    return;
                }

                if (i.customId  === `button1`) {
                    member.roles.add(role1);
                    i.reply({ content: `Dir wurde folgende Rolle gegeben: **${role1.name}**`, ephemeral: true});
                }

                if (i.customId  === `button2`) {
                    member.roles.add(role2);
                    i.reply({ content: `Dir wurde folgende Rolle gegeben: **${role2.name}**`, ephemeral: true});
                }
                
                if (i.customId  === `button3`) {
                    member.roles.add(role3);
                    i.reply({ content: `Dir wurde folgende Rolle gegeben: **${role3.name}**`, ephemeral: true});
                }

                if (i.customId  === `button4`) {
                    member.roles.add(role4);
                    i.reply({ content: `Dir wurde folgende Rolle gegeben: **${role4.name}**`, ephemeral: true});
                }

                if (i.customId  === `button5`) {
                    member.roles.add(role5);
                    i.reply({ content: `Dir wurde folgende Rolle gegeben: **${role5.name}**`, ephemeral: true});
                }
            })
    } 
}