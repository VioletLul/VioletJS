const { SlashCommandBuilder } = require("@discordjs/builders");
const { PermissionsBitField, ButtonStyle, ActionRowBuilder, EmbedBuilder, ButtonBuilder } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
    .setName('reactionrole')
    .setDescription('Reaktionsrollen Befehl')
    .addRoleOption(option => option.setName(`role1`).setDescription("Diese Rolle wählst du als erstes aus.").setRequired(true)),
    async execute (interaction, client) {

        const role1 = interaction.options.getRole('role1');
 if (!interaction.member.permissions.has(PermissionsBitField.Flags.Administrator)) return await interaction.reply({ content: "Hm, anscheinend bist du nicht mächtig genug um diese Nachricht zu beschwören!", ephemeral: true});
       
        
        const button = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId("button1")
            .setLabel(`${role1.name}`)
            .setStyle(ButtonStyle.Secondary)
        )
         
        const embed = new EmbedBuilder()
            .setColor("Blurple")
            .setTitle("Rollen? ROLLEN!")
            .setDescription(`Da, schau nach unten`)
            
            await interaction.reply({ embeds: [embed], components: [button]});

            const collector = await interaction.channel.createMessageComponentCollector();

            collector.on('collect', async (i) =>{

                const member = i.member;

                if (i.guild.members.me.roles.highest.position < role1.position) {
                    i.update({ content: "Die Rollen die ich vergeben soll sind zu mächtig. Ich habe diese Reaktion bis zum beheben des Fehlers ausgeschaltet.", ephemeral: true});
                    return;
                }

                if (i.customId  === `button1`) {
                    member.roles.add(role1);
                    i.reply({ content: `Dir wurde folgende Rolle gegeben: **${role1.name}**`, ephemeral: true});
                }
            })
    } 
}