const { SlashCommandBuilder } = require("@discordjs/builders");
const { PermissionsBitField } = require("discord.js");

module.exports = {
    data: new SlashCommandBuilder()
    .setName("online")
    .setDescription("Status Command"),
    async execute(interaction, client) {
        if (!interaction.member.permissions.has(PermissionsBitField.Flags.Administrator)) return await interaction.reply({ content: "Hm, anscheinend bist du nicht mächtig genug um diese Nachricht zu beschwören!", ephemeral: true});
        await interaction.reply({ content: "💚 Das System ist online"});
    }
}